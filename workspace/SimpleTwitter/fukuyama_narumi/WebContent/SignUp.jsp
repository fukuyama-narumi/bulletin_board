<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
                <br /> 
                <label for="name">ログインID</label> 
                <input name="login_id" id="login_id" />
                <br />
                <label for="name">パスワード</label> 
                <input name="password"id="password" /> 
                <br /> 
                <label for="name">名称</label> 
                <input name="name" id="name" /> 
                <br /> 
                <select name = "branch">
				<option value="1">本社</option>
				<option value="2">支店A</option>
				<option value="3">支店B</option>
				<option value="4">支店C</option>
				</select> 
				<select name = "position">
				<option value="1">総務人事</option>
				<option value="2">情報管理</option>
				<option value="3">社員</option>
				</select> 
				<br /> 
				<input type="submit" value="登録" /> 
				<br /> 
				<a href="./">戻る</a>
            </form>
            <div class="copyright">Copyright(c)Your Name</div>
        </div>
    </body>
</html>